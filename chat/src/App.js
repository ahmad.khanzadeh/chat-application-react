import {ChatEngine} from 'react-chat-engine';
import './App.css';
import ChatFeed from './components/ChatFeed';

const App = () => {
    return(
        <ChatEngine 
            height="100vh"
            projectID="d6c8cf19-c1f9-4734-a14d-db9c1f7f1355"
            userName="Mina"
            userSecret="09196460843"
            // to render users dynamically from a list( log in and log out from chat Profile) props must be always in { }
             renderChatFeed={(chatAppProps) => <ChatFeed {...chatAppProps}/>}
             
        />
    )
}

export default App;