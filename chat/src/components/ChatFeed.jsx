import React from "react";
import MessageForm from './MessageForm';
import MyMessage from './MyMessage';
import TheirMessage from './TheirMessage';


const ChatFeed = (props)=>{
    // console.log(props);
    const {chats,activeChat,useName,message} =props;
    // what is our current chat -if chat exist then find active chat
    const chat= chats && chats[activeChat];
    //  a function which generate messages 
    const renderMessages = ()=>{
            // fetch messages( get id of messages from server )
            const keys=Object.keys(message);
            // render messages from keys ( each single key )
            return keys.map((key,index)=>{
                // get message
                const message= message[key];
                // check if it is the last message
                const lastMessageKey= index===0? null : keys[index -1];
                // is it for the current user message?
                const isMyMessage = useName===message.sender.username;

                return(
                    // act like our message 
                    <div key={`msg_${index}`} style={{width:'100%'}}>
                            <div className="message-block">
                                {/* display messages based on  */}
                                {
                                    isMyMessage ?  <MyMessage message={message}/> : <TheirMessage message={message} lastMessage={message[lastMessageKey]}/>
                                }
                            </div>
                            <div className="read-receipts" style={{marginRight: isMyMessage ? '18px' : '0px',marginLeft: isMyMessage ? '0px' : '68px'}}>
                                    read-receipts --chat bubbles should be displayed here
                            </div>
                    </div>
                );
            })
    }

    

    if(!chat) return 'Loading ... ';
    // rendering the structure of chat feed
    return(
        <div className='chat-feed'>
            <div className='chat-title-container'>
                <div className='chat-title'>
                    {/* make sure that we have the chat and then search for title */}
                    {chat?.title}
                </div>
                <div className='chat-subtitle'>
                    {/* get specific person */}
                    {chat.people.map((person) =>`${person.person.username}`)}
                </div>
            </div>
            {/* since chat is now finished, render chat box */}
            {renderMessages()}
            <div style={{height: '100px'}}/>
            {/* a form which user can send messages */}
            <div className="message-form-container">
                    <MessageForm {...props} chatId={activeChat}/>
            </div>
        </div>
    );
}

export default ChatFeed;

// ta 30 daghighe 

